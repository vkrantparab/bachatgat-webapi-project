CREATE EXTENSION "uuid-ossp";

CREATE TABLE Users(
	Id uuid DEFAULT uuid_generate_v4() not null,
	FirstName text NULL,
	LastName text NULL,
	Email text NULL,
	CreatedAt text NULL,
	Address text NULL,
	Address2 text NULL,
	City text NULL,
	State text NULL,
	Postal text NULL,
	Phone text NULL,
	PhoneExt text NULL,
	LatestEntryMonth text NULL,
	LatestEntryYear text NULL
);

CREATE TABLE Entries(
	Id uuid DEFAULT uuid_generate_v4() not null,
	UserId uuid NOT NULL,
	FullName text NULL,
	FixedContribution float NULL,
	LoanReturned float NULL,
	LoanIntrestApplicable float NULL,
	Fine float NULL,
	Others float NULL,
	TotalIncomingAmount float NULL,
	LoanTaken float NULL,
	LoanIntrestReturned float NULL,
	TotalOutGoingAmount float NULL,
	LoanSettlement float NULL,
	Month text NULL,
	Year text NULL,
	CreatedAt text NULL
);

alter table Users add constraint users_pkey primary key(Id);

alter table Entries add constraint entries_pkey primary key(Id);

alter table Entries add constraint entries_userid_fkey foreign key(UserId) 
references Users(Id);