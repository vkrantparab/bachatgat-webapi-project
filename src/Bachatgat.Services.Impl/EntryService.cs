﻿using Bachatgat.Services.DataContracts;
using Bachatgat.Services.Impl.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bachatgat.Services.Impl
{
    public class EntryService : IEntryService
    {
        async Task<Entry> IEntryService.CreateEntryAsync(Entry entry)
        {
            Entities.Entry entryEntity = EntryMapper.Map(entry);
            entryEntity.CreatedAt = DateTime.UtcNow.ToString("o");
            Entities.Entry newEntry = this._dbContext.Entries.Add(entryEntity).Entity;

            var userEntity = this._dbContext.Users.Where(u => u.Id == entryEntity.UserId).FirstOrDefault();
            userEntity.LatestEntryMonth = entryEntity.Month;
            userEntity.LatestEntryYear = entryEntity.Year;
            Entities.User updatedUserEntry = this._dbContext.Users.Update(userEntity).Entity;

            await _dbContext.SaveChangesAsync();

            return EntryMapper.Map(newEntry);

        }

        async Task<PagedList<Entry>> IEntryService.GetEntriesAsync()
        {
            var skip = 0;
            var take = 1000;
            IQueryable<Entities.Entry> entriesQuery = _dbContext.Entries.OrderBy(p => p.Year);

            int totalRecordCount = _dbContext.Entries.Count();
            IEnumerable<Entities.Entry> entries = await entriesQuery.Skip(skip).Take(take).ToListAsync();

            DataContracts.PagedList<DataContracts.Entry> pagedList = new DataContracts.PagedList<DataContracts.Entry>(EntryMapper.MapCollection(entries), totalRecordCount, skip, take);

            return pagedList;
        }

        async Task<PagedList<Entry>> IEntryService.GetMonthlyEntriesAsync(string month)
        {
            var skip = 0;
            var take = 1000;
            IQueryable<Entities.Entry> entriesQuery = _dbContext.Entries.Where(p=>p.Month == month).OrderBy(p => p.Year);

            int totalRecordCount = _dbContext.Entries.Count();
            IEnumerable<Entities.Entry> entries = await entriesQuery.Skip(skip).Take(take).ToListAsync();

            DataContracts.PagedList<DataContracts.Entry> pagedList = new DataContracts.PagedList<DataContracts.Entry>(EntryMapper.MapCollection(entries), totalRecordCount, skip, take);

            return pagedList;
        }

        async Task<PagedList<Entry>> IEntryService.GetUserEntriesAsync(Guid userId)
        {
            var skip = 0;
            var take = 1000;
            IQueryable<Entities.Entry> entriesQuery = _dbContext.Entries.Where(p => p.UserId == userId).OrderBy(p => p.Year);

            int totalRecordCount = _dbContext.Entries.Count();
            IEnumerable<Entities.Entry> entries = await entriesQuery.Skip(skip).Take(take).ToListAsync();

            DataContracts.PagedList<DataContracts.Entry> pagedList = new DataContracts.PagedList<DataContracts.Entry>(EntryMapper.MapCollection(entries), totalRecordCount, skip, take);

            return pagedList;
        }

        async Task<Entry> IEntryService.UpdateEntryAsync(Guid entryId, Entry entry)
        {
            if (entry == null)
            {
                throw new Exception("There is no Entry data");
            }

            Entities.Entry entryEntity = EntryMapper.Map(entry);
            Entities.Entry updatedEntry = _dbContext.Entries.Update(entryEntity).Entity;
            await _dbContext.SaveChangesAsync();

            return EntryMapper.Map(updatedEntry);
        }

        //async Task<Statistics> IEntryService.GetStatisticsAsync(string month, string year)
        //{
        //    if (string.IsNullOrEmpty(month))
        //    {
        //        throw new Exception("Can not fetch statics. Please specify month");
        //    }
        //    Statistics stats = new Statistics();

        //    var openingBalance = this._dbContext.Entries.Sum(e => e.FixedContribution);
        //    var entries = this._dbContext.Entries.Where(e => e.Month == month).ToAsyncEnumerable();
        //}

        public EntryService(IBachatgatDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        private readonly IBachatgatDBContext _dbContext;
    }
}
