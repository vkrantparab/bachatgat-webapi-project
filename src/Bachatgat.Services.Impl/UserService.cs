﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bachatgat.Services.DataContracts;
using Bachatgat.Services.Impl.Mappers;
using Microsoft.EntityFrameworkCore;

namespace Bachatgat.Services.Impl
{
    public class UserService : IUserService
    {
        async Task<User> IUserService.CreateUserAsync(User user)
        {
            Entities.User userEntity = UserMapper.Map(user);
            userEntity.CreatedAt = DateTime.UtcNow.ToString("o");

            Entities.User newUser = _dbContext.Users.Add(userEntity).Entity;
            await _dbContext.SaveChangesAsync();

            return UserMapper.Map(newUser);
        }

        async Task<PagedList<User>> IUserService.GetUsersAsync()
        {
            var skip = 0;
            var take = 1000;
            IQueryable<Entities.User> usersQuery = _dbContext.Users.OrderBy(p => p.FirstName);

            int totalRecordCount = _dbContext.Users.Count();
            IEnumerable<Entities.User> users = await usersQuery.Skip(skip).Take(take).ToListAsync();

            DataContracts.PagedList<DataContracts.User> pagedList = new DataContracts.PagedList<DataContracts.User>(UserMapper.MapCollection(users), totalRecordCount, skip, take);

            return pagedList;
        }

        async Task<User> IUserService.GetUserAsync(Guid userId)
        {
            if(userId == null)
            {
                throw new Exception("Please Provide UserId");
            }

            Entities.User newUser = await _dbContext.Users.Where(u => u.Id == userId).FirstOrDefaultAsync();

            if(newUser == null)
            {
                throw new Exception("No user found for given UserId");
            }

            return UserMapper.Map(newUser);
        }

        async Task<bool> IUserService.UpdateUserAsync(Guid userId, User user)
        {
            if(user == null)
            {
                throw new Exception("There is no User data");
            }

            Entities.User userEntity = UserMapper.Map(user);
            Entities.User updatedUser = _dbContext.Users.Update(userEntity).Entity;
            await _dbContext.SaveChangesAsync();
            return true;
        }

        UserDefaults IUserService.GetUserDefaultsAsync(Guid userId)
        {
            UserDefaults userDefaults = new UserDefaults();
            var userEntity = this._dbContext.Users.Where(u => u.Id == userId).FirstOrDefault();
            var entriesEntity = this._dbContext.Entries.Where(e => e.UserId == userId).ToList();

            var totalLoanTaken = entriesEntity.Sum(e => e.LoanTaken);
            var totalLoanReturned = entriesEntity.Sum(e => e.LoanReturned);

            userDefaults.FullName = userEntity.FirstName + " " + userEntity.LastName;
            userDefaults.UserId = userEntity.Id;
            userDefaults.RateOfIntrest = 2;
            userDefaults.PendingLoanAmount = Convert.ToSingle(totalLoanTaken - totalLoanReturned);
            userDefaults.ExpectedIntrest = userDefaults.PendingLoanAmount * 0.02f;

            return userDefaults;
        }

        public UserService(IBachatgatDBContext dbContext)
        {
            this._dbContext = dbContext;
        }

        private readonly IBachatgatDBContext _dbContext;

    }
}
