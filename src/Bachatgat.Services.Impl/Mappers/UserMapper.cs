﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bachatgat.Services.Impl.Mappers
{
    public static class UserMapper
    {
        // Entity to DataContract mapping
        public static DataContracts.User Map(Entities.User user)
        {
            return new DataContracts.User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                CreatedAt = user.CreatedAt,
                Address = user.Address,
                Address2 = user.Address2,
                City = user.City,
                State = user.State,
                Postal = user.Postal,
                Phone = user.Phone,
                PhoneExt = user.PhoneExt,
                LatestEntryMonth = user.LatestEntryMonth,
                LatestEntryYear = user.LatestEntryYear,
            };
        }

        // DataContract to Entity mapping
        public static Entities.User Map(DataContracts.User user)
        {
            return new Entities.User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                CreatedAt = user.CreatedAt,
                Address = user.Address,
                Address2 = user.Address2,
                City = user.City,
                State = user.State,
                Postal = user.Postal,
                Phone = user.Phone,
                PhoneExt = user.PhoneExt,
                LatestEntryMonth = user.LatestEntryMonth,
                LatestEntryYear = user.LatestEntryYear,
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<DataContracts.User> MapCollection(IEnumerable<Entities.User> users)
        {
            return from u in users
                   select Map(u);
        }
    }
}
