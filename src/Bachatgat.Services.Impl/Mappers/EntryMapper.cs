﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bachatgat.Services.Impl.Mappers
{
    public static class EntryMapper
    {
        // Entity to DataContract mapping
        public static DataContracts.Entry Map(Entities.Entry entry)
        {
            return new DataContracts.Entry()
            {
                Id = entry.Id,
                UserId = entry.UserId,
                FullName = entry.FullName,
                FixedContribution = Convert.ToSingle(entry.FixedContribution),
                LoanReturned = Convert.ToSingle(entry.LoanReturned),
                LoanIntrestApplicable = Convert.ToSingle(entry.LoanIntrestApplicable),
                Fine = Convert.ToSingle(entry.Fine),
                Others = Convert.ToSingle(entry.Others),
                TotalIncomingAmount = Convert.ToSingle(entry.TotalIncomingAmount),
                LoanTaken = Convert.ToSingle(entry.LoanTaken),
                LoanIntrestReturned = Convert.ToSingle(entry.LoanIntrestReturned),
                TotalOutGoingAmount = Convert.ToSingle(entry.TotalOutGoingAmount),
                LoanSettlement = Convert.ToSingle(entry.LoanSettlement),
                Month = entry.Month,
                Year = entry.Year,
                CreatedAt = entry.CreatedAt
            };
        }

        // DataContract to Entity mapping
        public static Entities.Entry Map(DataContracts.Entry entry)
        {
            return new Entities.Entry()
            {
                Id = entry.Id,
                UserId = entry.UserId,
                FullName = entry.FullName,
                FixedContribution = entry.FixedContribution,
                LoanReturned = entry.LoanReturned,
                LoanIntrestApplicable = entry.LoanIntrestApplicable,
                Fine = entry.Fine,
                Others = entry.Others,
                TotalIncomingAmount = entry.FixedContribution + entry.Fine + entry.LoanReturned + entry.LoanIntrestApplicable + entry.Others,
                LoanTaken = entry.LoanTaken,
                LoanIntrestReturned = entry.LoanIntrestReturned,
                TotalOutGoingAmount = entry.LoanTaken + entry.LoanIntrestReturned,
                LoanSettlement = entry.LoanSettlement,
                Month = entry.Month,
                Year = entry.Year,
                CreatedAt = entry.CreatedAt
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<DataContracts.Entry> MapCollection(IEnumerable<Entities.Entry> entries)
        {
            return from u in entries
                   select Map(u);
        }
    }
}
