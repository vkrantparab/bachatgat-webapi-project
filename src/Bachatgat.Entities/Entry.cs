﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bachatgat.Entities
{
    [Table("Entries")]
    public class Entry
    {
        [Key]
        public Guid Id { get; set; }
        [ForeignKey("User")]
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public double FixedContribution { get; set; }
        public double LoanReturned { get; set; }
        public double LoanIntrestApplicable { get; set; }
        public double Fine { get; set; }
        public double Others { get; set; }
        public double TotalIncomingAmount { get; set; }
        public double LoanTaken { get; set; }
        public double LoanIntrestReturned { get; set; }
        public double TotalOutGoingAmount { get; set; }
        public double LoanSettlement { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string CreatedAt { get; set; }
    }
}
