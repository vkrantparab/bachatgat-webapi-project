﻿using Bachatgat.Services.DataContracts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bachatgat.Services
{
    public interface IEntryService
    {
        Task<PagedList<Entry>> GetEntriesAsync();
        Task<PagedList<Entry>> GetMonthlyEntriesAsync(string month);
        Task<PagedList<Entry>> GetUserEntriesAsync(Guid userId);
        Task<Entry> CreateEntryAsync(Entry entry);
        Task<Entry> UpdateEntryAsync(Guid entryId, Entry entry);
        //Task<Statistics> GetStatisticsAsync(string month, string year);
    }
}
