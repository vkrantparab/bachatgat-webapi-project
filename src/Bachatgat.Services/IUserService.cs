﻿using Bachatgat.Services.DataContracts;
using System;
using System.Threading.Tasks;

namespace Bachatgat.Services
{
    public interface IUserService
    {
        Task<PagedList<User>> GetUsersAsync();

        Task<User> GetUserAsync(Guid userId);

        UserDefaults GetUserDefaultsAsync(Guid userId);

        Task<User> CreateUserAsync(User user);

        Task<bool> UpdateUserAsync(Guid userId, User user);
    }
}
