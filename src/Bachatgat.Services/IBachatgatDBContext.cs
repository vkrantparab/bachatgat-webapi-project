﻿using Microsoft.EntityFrameworkCore;
using Bachatgat.Entities;
using System.Threading.Tasks;

namespace Bachatgat.Services
{
    public interface IBachatgatDBContext
    {
        DbSet<Entry> Entries { get; set; }
        DbSet<User> Users { get; set; }

        Task<int> SaveChangesAsync();
    }
}
