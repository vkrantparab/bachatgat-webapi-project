﻿using System;

namespace Bachatgat.Services.DataContracts
{
    public class Entry
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public float FixedContribution { get; set; }
        public float LoanReturned { get; set; }
        public float LoanIntrestApplicable { get; set; }
        public float Fine { get; set; }
        public float Others { get; set; }
        public float TotalIncomingAmount { get; set; }
        public float LoanTaken { get; set; }
        public float LoanIntrestReturned { get; set; }
        public float TotalOutGoingAmount { get; set; }
        public float LoanSettlement { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public string CreatedAt { get; set; }
    }
}
