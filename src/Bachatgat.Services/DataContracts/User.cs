﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bachatgat.Services.DataContracts
{
    public class User
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string CreatedAt { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postal { get; set; }
        public string Phone { get; set; }
        public string PhoneExt { get; set; }
        public string LatestEntryMonth { get; set; }
        public string LatestEntryYear { get; set; }
    }
}
