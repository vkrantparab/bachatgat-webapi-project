﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bachatgat.Services.DataContracts
{
    public class PagedList<T>
    {
        public IEnumerable<T> Items { get; }
        public int TotalItems { get; }
        public int Skip { get; }
        public int Take { get; }

        public PagedList(IEnumerable<T> items, int totalItems, int skip, int take)
        {
            Items = items;
            TotalItems = totalItems;
            Skip = skip;
            Take = take;
        }

        public PagedList() { }
    }
}
