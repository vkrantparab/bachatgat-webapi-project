﻿using System;

namespace Bachatgat.WebAPI.Models
{
    public class UserDefaults
    {
        public Guid UserId { get; set; }
        public string FullName { get; set; }
        public float ExpectedIntrest { get; set; }
        public float RateOfIntrest { get; set; }
        public float PendingLoanAmount { get; set; }
    }                                                                                                                               
}