﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI.Models
{
    public class Statistics
    {
        public float OpeningBankBalance { get; set; }
        public float MonthlyCollection { get; set; }
        public float MonthlyLoanCollection { get; set; }
        public float MonthlyIntrestCollection { get; set; }
        public float MonthlyFineCollection { get; set; }
        public float Profit { get; set; }
        public float IntrestFromBank { get; set; }
        public float OtherCollection { get; set; }
        public float LoanTakenFromBank { get; set; }
        public float TotalCollection { get; set; }

        public float LoanGiven { get; set; }
        public float StationeryExpenses { get; set; }
        public float HospitalityExpenses { get; set; }
        public float TravellingExpenses { get; set; }
        public float PrincipalReturnToBank { get; set; }
        public float IntrestReturnToBank { get; set; }
        public float MembersIntrestSaving { get; set; }
        public float ProfitShared { get; set; }
        public float OtherExpenses { get; set; }
        public float ClosingBankBalance { get; set; }
        public float TotalExpenses { get; set; }
    }
}
