﻿using Bachatgat.WebAPI.Models;
using Bachatgat.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI.Mappers
{
    public static class UserMapper
    {
        // Model to DataContract mapping
        public static Services.DataContracts.User Map(User user)
        {
            return new Services.DataContracts.User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                CreatedAt = user.CreatedAt,
                Address = user.Address,
                Address2 = user.Address2,
                City = user.City,
                State = user.State,
                Postal = user.Postal,
                Phone = user.Phone,
                PhoneExt = user.PhoneExt,
                LatestEntryMonth = user.LatestEntryMonth,
                LatestEntryYear = user.LatestEntryYear
            };
        }

        // DataContract to Model mapping
        public static User Map(Services.DataContracts.User user)
        {
            return new User()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                CreatedAt = user.CreatedAt,
                Address = user.Address,
                Address2 = user.Address2,
                City = user.City,
                State = user.State,
                Postal = user.Postal,
                Phone = user.Phone,
                PhoneExt = user.PhoneExt,
                LatestEntryMonth = user.LatestEntryMonth,
                LatestEntryYear = user.LatestEntryYear
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<User> MapCollection(IEnumerable<Services.DataContracts.User> projects)
        {
            return from u in projects
                   select Map(u);
        }
    }
}
