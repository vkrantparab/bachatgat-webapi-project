﻿using Bachatgat.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI.Mappers
{
    public class UserDefaultsMapper
    {
        // Model to DataContract mapping
        public static Services.DataContracts.UserDefaults Map(UserDefaults user)
        {
            return new Services.DataContracts.UserDefaults()
            {
                UserId = user.UserId,
                FullName = user.FullName,
                ExpectedIntrest = user.ExpectedIntrest,
                RateOfIntrest = user.RateOfIntrest,
                PendingLoanAmount = user.PendingLoanAmount
            };
        }

        // DataContract to Model mapping
        public static UserDefaults Map(Services.DataContracts.UserDefaults user)
        {
            return new UserDefaults()
            {
                UserId = user.UserId,
                FullName = user.FullName,
                ExpectedIntrest = user.ExpectedIntrest,
                RateOfIntrest = user.RateOfIntrest,
                PendingLoanAmount = user.PendingLoanAmount
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<UserDefaults> MapCollection(IEnumerable<Services.DataContracts.UserDefaults> projects)
        {
            return from u in projects
                   select Map(u);
        }
    }
}
