﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI.Mappers
{
    public static class EntryMapper
    {
        // Model to DataContract mapping
        public static Services.DataContracts.Entry Map(Models.Entry entry)
        {
            return new Services.DataContracts.Entry()
            {
                Id = entry.Id,
                UserId = entry.UserId,
                FullName = entry.FullName,
                FixedContribution = entry.FixedContribution,
                LoanReturned = entry.LoanReturned,
                LoanIntrestApplicable = entry.LoanIntrestApplicable,
                Fine = entry.Fine,
                Others = entry.Others,
                LoanTaken = entry.LoanTaken,
                LoanIntrestReturned = entry.LoanIntrestReturned,
                LoanSettlement = entry.LoanSettlement,
                Month = entry.Month,
                Year = entry.Year,
                CreatedAt = entry.CreatedAt
            };
        }

        // DataContract to Entity mapping
        public static Models.Entry Map(Services.DataContracts.Entry entry)
        {
            return new Models.Entry()
            {
                Id = entry.Id,
                UserId = entry.UserId,
                FullName = entry.FullName,
                FixedContribution = entry.FixedContribution,
                LoanReturned = entry.LoanReturned,
                LoanIntrestApplicable = entry.LoanIntrestApplicable,
                Fine = entry.Fine,
                Others = entry.Others,
                TotalIncomingAmount = entry.FixedContribution + entry.LoanReturned + entry.LoanIntrestApplicable + entry.Fine + entry.Others,
                LoanTaken = entry.LoanTaken,
                LoanIntrestReturned = entry.LoanIntrestReturned,
                TotalOutGoingAmount = entry.LoanTaken + entry.LoanIntrestReturned,
                LoanSettlement = entry.LoanSettlement,
                Month = entry.Month,
                Year = entry.Year,
                CreatedAt = entry.CreatedAt
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<Models.Entry> MapCollection(IEnumerable<Services.DataContracts.Entry> entries)
        {
            return from e in entries
                   select Map(e);
        }
    }
}
