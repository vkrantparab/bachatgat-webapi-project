﻿using Bachatgat.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI.Mappers
{
    public static class StatisticsMapper
    {
        // Model to DataContract mapping
        public static Services.DataContracts.Statistics Map(Statistics stats)
        {
            return new Services.DataContracts.Statistics()
            {
                OpeningBankBalance = stats.OpeningBankBalance,
                MonthlyCollection = stats.MonthlyCollection,
                MonthlyLoanCollection = stats.MonthlyLoanCollection,
                MonthlyIntrestCollection = stats.MonthlyIntrestCollection,
                MonthlyFineCollection = stats.MonthlyFineCollection,
                Profit = stats.Profit,
                IntrestFromBank = stats.IntrestFromBank,
                OtherCollection = stats.OtherCollection,
                LoanTakenFromBank = stats.LoanTakenFromBank,
                TotalCollection = stats.TotalCollection,
                LoanGiven = stats.LoanGiven,
                StationeryExpenses = stats.StationeryExpenses,
                HospitalityExpenses = stats.HospitalityExpenses,
                TravellingExpenses = stats.TravellingExpenses,
                PrincipalReturnToBank = stats.PrincipalReturnToBank,
                IntrestReturnToBank = stats.IntrestReturnToBank,
                MembersIntrestSaving = stats.MembersIntrestSaving,
                ProfitShared = stats.ProfitShared,
                OtherExpenses = stats.OtherExpenses,
                ClosingBankBalance = stats.ClosingBankBalance,
                TotalExpenses = stats.TotalExpenses
            };
        }

        // DataContract to Model mapping
        public static Statistics Map(Services.DataContracts.Statistics stats)
        {
            return new Statistics()
            {
                OpeningBankBalance = stats.OpeningBankBalance,
                MonthlyCollection = stats.MonthlyCollection,
                MonthlyLoanCollection = stats.MonthlyLoanCollection,
                MonthlyIntrestCollection = stats.MonthlyIntrestCollection,
                MonthlyFineCollection = stats.MonthlyFineCollection,
                Profit = stats.Profit,
                IntrestFromBank = stats.IntrestFromBank,
                OtherCollection = stats.OtherCollection,
                LoanTakenFromBank = stats.LoanTakenFromBank,
                TotalCollection = stats.TotalCollection,
                LoanGiven = stats.LoanGiven,
                StationeryExpenses = stats.StationeryExpenses,
                HospitalityExpenses = stats.HospitalityExpenses,
                TravellingExpenses = stats.TravellingExpenses,
                PrincipalReturnToBank = stats.PrincipalReturnToBank,
                IntrestReturnToBank = stats.IntrestReturnToBank,
                MembersIntrestSaving = stats.MembersIntrestSaving,
                ProfitShared = stats.ProfitShared,
                OtherExpenses = stats.OtherExpenses,
                ClosingBankBalance = stats.ClosingBankBalance,
                TotalExpenses = stats.TotalExpenses
            };
        }

        //Mapping to send only fields required to show list of projects.
        public static IEnumerable<Statistics> MapCollection(IEnumerable<Services.DataContracts.Statistics> projects)
        {
            return from u in projects
                   select Map(u);
        }
    }
}
