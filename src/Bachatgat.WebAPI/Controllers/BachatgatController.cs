﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bachatgat.Services;
using Bachatgat.WebAPI.Mappers;
using Bachatgat.WebAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace Bachatgat.WebAPI.Controllers
{
    [Route("api/BachatgatServices")]
    [ApiController]
    public class BachatgatController : Controller
    {
        #region Users APIs
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var userPagedList = await _userService.GetUsersAsync();
                PagedList<User> pagedListWithUserModel = new PagedList<User>(UserMapper.MapCollection(userPagedList.Items), userPagedList.TotalItems, userPagedList.Skip, userPagedList.Take);

                return Json(pagedListWithUserModel);
            }
            catch(Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpGet("users/{userId}")]
        public async Task<IActionResult> GetUser(Guid userId)
        {
            try
            {
                var user = await _userService.GetUserAsync(userId);
                
                return Json(UserMapper.Map(user));
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpGet("users/{userId}/defaults")]
        public IActionResult GetUserDefaults(Guid userId)
        {
            try
            {
                var userDefaults = _userService.GetUserDefaultsAsync(userId);

                return Json(UserDefaultsMapper.Map(userDefaults));
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        // POST projects/5/requestQuote
        [HttpPost("users")]
        public async Task<IActionResult> CreateUser([FromBody]User user)
        {
            try
            {
                var userContract = UserMapper.Map(user);
                var newUser = await this._userService.CreateUserAsync(userContract);
            
                return Json(UserMapper.Map(newUser));
            }
            catch(Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpPut("users/{userId}")]
        public async Task<IActionResult> UpdateUser(Guid userId, [FromBody]User user)
        {
            try
            {
                var userContract = UserMapper.Map(user);
                var isUpdated = await this._userService.UpdateUserAsync(userId, userContract);

                return Json(isUpdated);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }
        #endregion

        #region Entries APIs
        [HttpGet("entries")]
        public async Task<IActionResult> GetEntries()
        {
            try
            {
                var entriesPagedList = await _entryService.GetEntriesAsync();
                PagedList<Entry> pagedListWithEntryModel = new PagedList<Entry>(EntryMapper.MapCollection(entriesPagedList.Items), entriesPagedList.TotalItems, entriesPagedList.Skip, entriesPagedList.Take);

                return Json(pagedListWithEntryModel);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpGet("entries/{userId}")]
        public async Task<IActionResult> GetUserEntries(Guid userId)
        {
            try
            {
                var userEntries = await _entryService.GetUserEntriesAsync(userId);

                PagedList<Entry> pagedListWithEntryModel = new PagedList<Entry>(EntryMapper.MapCollection(userEntries.Items), userEntries.TotalItems, userEntries.Skip, userEntries.Take);

                return Json(pagedListWithEntryModel);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpGet("entries/{month}")]
        public async Task<IActionResult> GetMonthlyEntries(string month)
        {
            try
            {
                var monthlyEntries = await _entryService.GetMonthlyEntriesAsync(month);

                PagedList<Entry> pagedListWithEntryModel = new PagedList<Entry>(EntryMapper.MapCollection(monthlyEntries.Items), monthlyEntries.TotalItems, monthlyEntries.Skip, monthlyEntries.Take);

                return Json(pagedListWithEntryModel);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        // POST projects/5/requestQuote
        [HttpPost("entries")]
        public async Task<IActionResult> CreateEntry([FromBody]Entry entry)
        {
            try
            {
                var entryContract = EntryMapper.Map(entry);
                var newEntry = await this._entryService.CreateEntryAsync(entryContract);

                return Json(EntryMapper.Map(newEntry));
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        [HttpPut("entries/{entryId}")]
        public async Task<IActionResult> UpdateEntry(Guid entryId, [FromBody]Entry entry)
        {
            try
            {
                var entryContract = EntryMapper.Map(entry);
                var isUpdated = await this._entryService.UpdateEntryAsync(entryId, entryContract);

                return Json(isUpdated);
            }
            catch (Exception ex)
            {
                return ReturnError(ex.Message);
            }
        }

        #endregion

        public BachatgatController(IUserService userService, IEntryService entryService)
        {
            this._userService = userService;
            this._entryService = entryService;
        }

        private IActionResult ReturnError(string message)
        {
            ModelState.AddModelError("ErrorMessages", message);
            return BadRequest(ModelState);
        }

        private readonly IUserService _userService;
        private readonly IEntryService _entryService;
    }
}