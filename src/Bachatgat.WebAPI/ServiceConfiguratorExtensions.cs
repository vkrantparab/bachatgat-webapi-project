﻿using Bachatgat.Services;
using Bachatgat.Services.Impl;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bachatgat.WebAPI
{
    public static class ServiceConfiguratorExtensions
    {
        public static IServiceCollection ConfigureSnoCalcNextServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<BachatgatDBContext>(options => options.UseNpgsql(configuration.GetConnectionString("BachatgatDatabase")));

            services.AddScoped(typeof(IBachatgatDBContext), typeof(BachatgatDBContext));
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IEntryService, EntryService>();
            return services;
        }
    }
}
