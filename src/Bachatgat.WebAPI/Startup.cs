using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Bachatgat.WebAPI
{
    public class Startup
    {
        public const string AppS3BucketKey = "AppS3Bucket";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static IConfiguration Configuration { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("_myAllowSpecificOrigins",
                builder =>
                {
                    builder.WithOrigins("http://localhost:4200",
                                        "http://localhost:4200/",
                                        "https://bachatgat.firebaseapp.com",
                                        "https://bachatgat.firebaseapp.com/",
                                        "https://bachatgat.web.app",
                                        "https://bachatgat.web.app/", 
                                        "http://192.168.1.203:4200", 
                                        "http://192.168.1.203:4200/").AllowAnyHeader().AllowAnyMethod();
                });
            });

            services.ConfigureSnoCalcNextServices(Configuration);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add S3 to the ASP.NET Core dependency injection framework.
            services.AddAWSService<Amazon.S3.IAmazonS3>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.Use(async (context, nextMiddleware) =>
            {
                context.Response.Headers.Add("Cache-Control", "no-cache");
                context.Response.Headers.Add("Pragma", "no-cache");
                context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                context.Response.Headers.Add("Access-Control-Allow-Methods", "*");
                await nextMiddleware();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseCors("_myAllowSpecificOrigins");

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
